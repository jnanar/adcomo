package main

import (
	"image/color"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/data/validation"
	"fyne.io/fyne/v2/driver/mobile"
	"fyne.io/fyne/v2/widget"
)

const defaultPlaceHolder string = "(Select one)"

type selectEntryValidated struct {
	widget.BaseWidget
	Select    *widget.Select
	Validator fyne.StringValidator `json:"-"`
}

func NewSelectEntryValidated(options []string, changed func(string)) *selectEntryValidated {
	// selectWidget := widget.NewSelect(options, changed)
	selectWidget := &widget.Select{
		OnChanged:   changed,
		Options:     options,
		PlaceHolder: defaultPlaceHolder,
	}
	selectWidget.ExtendBaseWidget(selectWidget)
	var e selectEntryValidated
	e.Select = selectWidget
	e.Validator = validation.NewRegexp("^$", "Category should not be empty")
	return &e
}

type numEntry struct {
	widget.Entry
}

func (n *numEntry) Keyboard() mobile.KeyboardType {
	return mobile.NumberKeyboard
}

func newNumEntry() *numEntry {
	e := &numEntry{}
	e.ExtendBaseWidget(e)
	e.Validator = validation.NewRegexp(`\d`, "Must contain a number")
	return e
}

func makeCell() fyne.CanvasObject {
	rect := canvas.NewRectangle(&color.NRGBA{128, 128, 128, 255})
	rect.SetMinSize(fyne.NewSize(5, 5))
	return rect
}
