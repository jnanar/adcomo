package main

import (
	"log"

	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

func getCategoryForm() *widget.Form {
	var cat CategoryList
	name := widget.NewEntry()
	name.SetPlaceHolder("Category Name")
	inactive := widget.NewCheck("Disabled check", func(res bool) {
		cat.Inactive = res
	})
	// sequence := newNumEntry()
	editForm := &widget.Form{
		Items: []*widget.FormItem{
			{Text: "name", Widget: name, HintText: "Title"},
			{Text: "archive", Widget: inactive, HintText: "Inactive"},
			// {Text: "sequence", Widget: sequence, HintText: "Must be an integer"},
		},
		OnCancel: func() {
			FormWindow.Close()
		},
		OnSubmit: func() {
			cat.Name = name.Text
			result := DB.Create(&cat)
			if result.Error != nil {
				log.Println("Can't create the category")
				return
			}
			// fyne.CurrentApp().SendNotification(&fyne.Notification{
			// 	Title: "Category created: " + name.Text,
			// })
			loadCategories()
			ac := widget.NewAccordion()
			AccordionPerCatID[cat.ID] = ac
			accordeonTabContent := getabContent(ac, ItemsPerCats[int(cat.ID)])
			AppTabs.Append(container.NewTabItem(cat.Name, container.NewVBox(accordeonTabContent)))
			FormWindow.Close()
		},
	}
	return editForm
}

func loadCategories() {
	var categories []CategoryList
	_read(&categories, 0)
	AllCategories = categories
	if len(CategoryNames) > 0 {
		CategoryNames = CategoryNames[:0]
	}
	for _, cat := range AllCategories {
		CategoryNames = append(CategoryNames, cat.Name)
		CategoryNameMap[cat.Name] = cat.ID // what happens if two category have the same name...
		CategoryObjMap[cat.ID] = cat
		categorySyncPerUUID[cat.SyncID] = cat
	}
}
