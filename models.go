package main

import (
	"gorm.io/gorm"
)

type CategoryList struct {
	gorm.Model
	ID       int64  // `json:"id" gorm:"primary_key"`
	Inactive bool   // `json:"inactive"`
	Name     string // `gorm:"type:varchar;not null" json:"name"`
	Sequence int    // `gorm:"type:int;not null" json:"sequence"`
	SyncID   string // `gorm:"type:varchar" json:"sync_id"`
}

// func catNameList(c []CategoryList) []string {
// 	var list []string
// 	for _, cat := range c {
// 		list = append(list, cat.Name)
// 	}
// 	return list
// }

type TodoItem struct {
	gorm.Model
	ID             int64  // `gorm:"primary_key"json:"id"`
	Inactive       bool   // `json:"inactive"`
	Name           string // `gorm:"type:varchar;not null" json:"name"`
	Content        string // `gorm:"type:varchar;not null" json:"content"`
	CategoryListID int64
	CategoryList   CategoryList
	Completed      bool
	Priority       int    // `json:"priority"`
	Sequence       int    // `gorm:"type:int;not null" json:"sequence"`
	SyncID         string // `gorm:"type:varchar" json:"sync_id"`
}

// func itemNameList(t []TodoItem) []string {
// 	var list []string
// 	for _, ti := range t {
// 		list = append(list, ti.Name)
// 	}
// 	return list
// }

type ReSequence struct {
	ParentId int64 `json:"parent_id"`
	RecordId int64 `json:"record_id" binding:"required"`
}

type ItemsPerCat struct {
	Category CategoryList
	Items    []TodoItem
}

// ///////////////////////////////////////////////////////////
// Sample data
func Populate(db *gorm.DB) {
	db.Create(Categories) // no batch insert
	db.Create(Items)
}

var Categories = []CategoryList{
	{Name: "Cat 1"},
	{Name: "Cat 2"},
	{Name: "Cat 3"},
	{Name: "Cat 4"},
}

var Items = []TodoItem{
	{Name: "Item 1", Content: "THIS IS CONTENT 1", CategoryListID: 1, Sequence: 1},
	{Name: "Item 2", Content: "THIS IS CONTENT 2", CategoryListID: 1, Sequence: 2},
	{Name: "Item 3", Content: "THIS IS CONTENT 3", CategoryListID: 2, Sequence: 3},
	{Name: "Item 4", Content: "THIS IS CONTENT 4", CategoryListID: 2, Sequence: 4},
	{Name: "Item 5", Content: "THIS IS CONTENT 5", CategoryListID: 2, Sequence: 5},
	{Name: "Item 6", Content: "THIS IS CONTENT 6", CategoryListID: 3, Sequence: 6},
	{Name: "Item 7", Content: "THIS IS CONTENT 6", CategoryListID: 3, Sequence: 7},
	{Name: "Item 8", Content: "THIS IS CONTENT 6", CategoryListID: 3, Sequence: 8},
	{Name: "Item 9", Content: "THIS IS CONTENT 6", CategoryListID: 3, Sequence: 9},
	{Name: "Item 10", Content: "THIS IS CONTENT 10", CategoryListID: 3, Sequence: 10},
	{Name: "Item 11", Content: "THIS IS CONTENT 11", CategoryListID: 3, Sequence: 11},
	{Name: "Item 12", Content: "THIS IS CONTENT 12", CategoryListID: 3, Sequence: 12},
	{Name: "Item 13", Content: "THIS IS CONTENT 13", CategoryListID: 3, Sequence: 13},
	{Name: "Item 14", Content: "THIS IS CONTENT 14", CategoryListID: 3, Sequence: 14},
	{Name: "Item 15", Content: "THIS IS CONTENT 15", CategoryListID: 3, Sequence: 15},
	{Name: "Item 16", Content: "THIS IS CONTENT 16", CategoryListID: 3, Sequence: 16},
}
