package main

import (
	"errors"
	"fmt"
	"log"
	"slices"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/widget"
)

func editItem(item TodoItem) *widget.Form {
	CurrentItem = item
	CurrentCategory = CategoryObjMap[int64(item.CategoryListID)]
	name := widget.NewEntry()
	name.SetPlaceHolder("Item Name")
	largeText := widget.NewMultiLineEntry()
	textContent := container.NewVScroll(largeText)
	textContent.SetMinSize(fyne.NewSize(0, 200))

	// selectEntryValidated := NewSelectEntryValidated(CategoryNames, func(c string) {
	// 	catID := CategoryNameMap[c]
	// 	CurrentItem.CategoryList = CategoryObjMap[catID]
	// 	CurrentItem.CategoryListID = int(catID)
	// 	fmt.Println("selected Category", c)
	// })
	selectEntry := widget.NewSelect(CategoryNames, func(c string) {
		catID := CategoryNameMap[c]
		CurrentItem.CategoryList = CategoryObjMap[catID]
		CurrentItem.CategoryListID = catID
	})
	cat, ok := CategoryObjMap[int64(CurrentItem.CategoryListID)]
	if !ok {
		log.Printf("No category for the following Item %s", CurrentItem.Name)
	}
	selectEntry.SetSelected(cat.Name)
	// ac, ok := AccordionPerCatID[cat.ID]
	// if !ok {
	// 	log.Println("Could not use the existing Accorein for Item %s", CurrentItem.Name)
	// }
	// plop := make(map[int]int)

	// for idx, it := range ac.Items {
	// 	plop[it.]
	// }

	disableCheck := widget.NewCheck("", func(inactive bool) {
		CurrentItem.Inactive = inactive
		fmt.Println("archive selected", inactive)
	})
	// seq := item.Sequence / 42
	// slideSequence := float64(item.Sequence) / 100.00
	// data := binding.BindFloat(&slideSequence)
	// slide := widget.NewSliderWithData(0, 1, data)
	// slide.Step = 0.01
	// Prefill data
	name.Text = CurrentItem.Name
	largeText.SetText(CurrentItem.Content)
	editForm := &widget.Form{
		Items: []*widget.FormItem{
			{Text: "name", Widget: name, HintText: "Title"},
			{Text: "content", Widget: textContent, HintText: "Item Description"},
			// {Text: "slide", Widget: slide, HintText: "Sequence"},
			{Text: "archive", Widget: disableCheck, HintText: "Inactive"},
			{Text: "Category", Widget: selectEntry, HintText: "Category Name"},
		},
		OnCancel: func() {
			FormWindow.Close()
		},
		OnSubmit: func() {
			var action string
			if item.ID != 0 {
				action = "update"
			} else {
				action = "create"
			}
			CurrentItem.Name = name.Text
			CurrentItem.Content = largeText.Text
			if CurrentItem.CategoryListID == 0 {
				err := errors.New("category is mandatory")
				dialog.ShowError(err, FormWindow)
				return
			}
			var res []TodoItem
			if action == "create" {
				result := DB.Create(&CurrentItem)
				if result.Error != nil {
					log.Println("Can't create the item")
					return
				}
			} else if action == "update" {
				_update(&res, CurrentItem.ID, CurrentItem)
			}
			// fyne.CurrentApp().SendNotification(&fyne.Notification{
			// 	Title:   "Form for: " + name.Text,
			// 	Content: largeText.Text,
			// })
			loadItems()
			cat, ok := CategoryObjMap[int64(CurrentItem.CategoryListID)]
			if !ok {
				log.Printf("No category for the follow ID %d", CurrentItem.CategoryListID)
			}
			if action == "create" {
				createAccordion(cat.ID)
			} else {
				updateAccordion(cat.ID)
			}
			FormWindow.Close()
		},
	}
	return editForm
}

func getCatAccordion(catID int64) *widget.Accordion {
	ac, ok := AccordionPerCatID[catID]
	if !ok {
		ac = widget.NewAccordion()
		AccordionPerCatID[catID] = ac
	}
	return ac
}

func createAccordion(catID int64) {
	ac := getCatAccordion(catID)

	content := getItemContent(ac, CurrentItem)
	accordeonContent := widget.NewAccordionItem(CurrentItem.Name, content)
	ac.Append(accordeonContent)
}

func updateAccordion(catID int64) {
	ac := getCatAccordion(catID)
	// we need item sequence = order otherwise we won't be able to use it.
	idx := getItemIndex(CurrentItem)
	acIt := ac.Items[idx]
	acIt.Title = CurrentItem.Name
	acIt.Detail = getItemContent(ac, CurrentItem)
	acIt.Detail.Refresh()
	ac.Close(idx)
}

func getItemIndex(item TodoItem) int {
	var items []TodoItem
	result := DB.Model(&TodoItem{}).Where("category_list_id = ?", item.CategoryListID).Order("sequence ASC, id").Find(&items)
	if result.Error != nil {
		log.Printf(LogError + result.Error.Error())
	}
	idx := 0
	for _, it := range items {
		if it.Inactive {
			continue
		}
		if it.ID == item.ID {
			break
		}
		idx += 1
	}
	if idx > len(items) {
		return -1
	}
	return idx
}

func getItemContent(ac *widget.Accordion, item TodoItem) *fyne.Container {
	border := makeCell()
	ent := widget.NewRichTextFromMarkdown(item.Content)
	ent.Scroll = container.ScrollNone
	ent.Truncation = fyne.TextTruncateOff
	ent.Wrapping = fyne.TextWrapWord
	textContent := container.NewStack(ent)
	middle := container.NewVBox(
		textContent,
		// widget.NewButton("Read", func() { readWindow(item) }),
		widget.NewButton("Update", func() { editWindow(item) }),
		&widget.Button{
			Text:       "Delete",
			Importance: widget.LowImportance,
			OnTapped: func() {
				cnf := dialog.NewConfirm("Confirmation", "Do you want to delete this item ?",
					func(response bool) {
						if response {
							// _delete("todo_items", int(item.ID))
							idx := getItemIndex(item)
							var res []TodoItem
							item.Inactive = true
							_update(&res, item.ID, item)
							ac.RemoveIndex(idx)
						}
					}, MainWindow)
				cnf.SetConfirmImportance(widget.DangerImportance)
				cnf.SetDismissText("No")
				cnf.SetConfirmText("Yes")
				cnf.Show()
			},
		},
	)
	content := container.NewBorder(nil, border, nil, nil, middle)
	return content
}

func getabContent(ac *widget.Accordion, items []TodoItem) *widget.Accordion {
	for _, item := range items {
		if item.Inactive {
			continue
		}
		content := getItemContent(ac, item)
		accordeonContent := widget.NewAccordionItem(item.Name, content)
		ac.Append(accordeonContent)
	}
	ac.MultiOpen = true
	return ac
}

func filterPerCat(catId int64, currentItem TodoItem) bool {
	return currentItem.CategoryListID == catId
}

func setItemsPerCat(categories []CategoryList, items []TodoItem) {
	var usedItemIds []int64
	for _, cat := range categories {
		for _, it := range items {
			if !slices.Contains(usedItemIds, it.ID) && filterPerCat(cat.ID, it) {
				ItemsPerCats[int(cat.ID)] = append(ItemsPerCats[int(cat.ID)], it)
				usedItemIds = append(usedItemIds, it.ID)
			}
		}

	}
}

func loadItems() {
	var items []TodoItem
	_read(&items, 0)
	AllItems = items
	for _, it := range AllItems {
		ItemObjMap[it.ID] = it
		itemSyncPerUUID[it.SyncID] = it
	}
}
