package main

import (
	"log"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

var (
	CategoryNames   []string
	CategoryNameMap = make(map[string]int64)
	CategoryObjMap  = make(map[int64]CategoryList)
	ItemObjMap      = make(map[int64]TodoItem)

	AccordionPerCatID = make(map[int64]*widget.Accordion)
	ItemsPerCats      = make(map[int][]TodoItem)
	CurrentItem       TodoItem
	CurrentCategory   CategoryList

	AllCategories []CategoryList
	AllItems      []TodoItem

	MainWindow fyne.Window
	FormWindow fyne.Window

	AppTabs *container.AppTabs
	ComoApp fyne.App
)

func actionWidow() {
	FormWindow = ComoApp.NewWindow("Actions")
	var newItem TodoItem
	editItemContent := editItem(newItem)
	CategoryContent := getCategoryForm()
	SyncFormContent := getSyncForm()
	CleanFormContent := getCleanForm()

	tabs := container.NewAppTabs(
		container.NewTabItem("New Item", editItemContent),
		container.NewTabItem("New Category", CategoryContent),
		container.NewTabItem("Sync", SyncFormContent),
		container.NewTabItem("Reset", CleanFormContent),
	)
	FormWindow.SetContent(tabs)
	FormWindow.CenterOnScreen()
	FormWindow.Show()
}

func readWindow(item TodoItem) {
	readWindow := ComoApp.NewWindow(item.Name)
	txtContent := widget.NewRichTextFromMarkdown(item.Content)
	txtContent.Scroll = container.ScrollBoth
	txtContent.Truncation = fyne.TextTruncateOff
	txtContent.Wrapping = fyne.TextWrapWord
	content := container.NewVSplit(
		txtContent,
		widget.NewButton("Close", func() {
			readWindow.Close()
		}),
	)
	content.Offset = 0.8
	readWindow.SetContent(content)
	readWindow.CenterOnScreen()
	readWindow.Show()
}

func editWindow(item TodoItem) {
	FormWindow = ComoApp.NewWindow("Add Data")
	editForm := editItem(item)
	FormWindow.SetContent(editForm)
	FormWindow.CenterOnScreen()
	FormWindow.Show()
}

func loadData() {
	loadCategories()
	loadItems()
}

func appRun() {
	log.Println("Start Drawing app")
	MainWindow = drawApp()
	MainWindow.SetMaster()
	MainWindow.CenterOnScreen()
	MainWindow.Resize(fyne.NewSize(400, 600))
	// comoApp.Settings().SetTheme(theme.LightTheme())
	MainWindow.ShowAndRun()
}

func drawTabs() {
	for _, cat := range AllCategories {
		log.Printf("Get content of tab %s", cat.Name)
		ac := getCatAccordion(cat.ID)
		accordeonTabContent := getabContent(ac, ItemsPerCats[int(cat.ID)])
		AppTabs.Append(container.NewTabItem(cat.Name, container.NewVBox(accordeonTabContent)))
	}
}

func drawApp() fyne.Window {
	loadData()
	setItemsPerCat(AllCategories, AllItems)
	MainWindow = ComoApp.NewWindow("Ad Como")
	AppTabs = container.NewAppTabs() // todo udate the £tabs pointer and use refresh without set content
	drawTabs()
	content := container.NewVScroll(
		container.NewVBox(
			AppTabs,
			&widget.Button{
				Text:       "Action",
				Importance: widget.HighImportance,
				OnTapped:   func() { actionWidow() },
			},
		),
	)

	// myWindow.SetContent(container.NewBorder(nil, container.New(layout.NewVBoxLayout(), add, exit), nil, nil, ac))
	MainWindow.SetContent(content)
	return MainWindow
}
