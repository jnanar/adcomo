package main

import (
	"log"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

func Unuse(plop interface{}) {
	_ = plop
}

func _read(atts interface{}, id int) interface{} {
	var result *gorm.DB
	if id == 0 {
		result = DB.Order("sequence ASC, id").Find(atts)
	} else {
		result = DB.Order("sequence ASC, id").Find(atts, id)
	}
	return result
}

func _update(atts interface{}, id int64, values interface{}) interface{} {
	result := DB.Find(atts, id)
	DB.Model(&atts).Where("id = ?", id).Select("*").Updates(values)
	return result
}

// func _delete(table string, id int) bool {
// 	if table == "category_lists" {
// 		rec := CategoryList{}
// 		DB.Table(table).Where("id = ?", id).Delete(&rec)
// 	} else if table == "todo_items" {
// 		rec := TodoItem{}
// 		DB.Table(table).Where("id = ?", id).Delete(&rec)
// 	}
// 	return true
// }

// func _resequence(model_name string, parentId int64, recordId int64) {
// 	// Resequence the items. We move one item on top of another.

// 	if model_name == "todo_item" {
// 		var results []TodoItem
// 		var moved_record TodoItem
// 		var parent_record TodoItem
// 		movedRecord := DB.Find(&moved_record, recordId)
// 		parentRecord := DB.Find(&parent_record, parentId)
// 		if parentRecord.Error != nil || movedRecord.Error != nil || movedRecord.RowsAffected == 0 {
// 			return
// 		}
// 		// Get all records ordered by sequence. We will put the movedRecord on top of the parentRecord
// 		// All records with sequence higher than the parentRecord are reorganized. Store all the records inside the result variable
// 		DB.Order("sequence asc").Where("category_list_id = ? AND id != ? AND sequence > ?", moved_record.CategoryListId, moved_record.ID, parent_record.Sequence).Find(&results)
// 		seq := parent_record.Sequence + 10
// 		moved_record.Sequence = seq
// 		DB.Model(&moved_record).Where("id = ?", moved_record.ID).Updates(&moved_record)
// 		for idx := range results {
// 			seq += 10
// 			DB.Model(&results).Where("id = ?", results[idx].ID).Update("Sequence", seq)
// 		}
// 		return
// 	}
// }

func _setSyncID() bool {
	var allCats []CategoryList
	result := DB.Order("sequence ASC, id").Find(&allCats)
	if result.Error != nil {
		return false
	}
	var allItems []TodoItem
	result = DB.Order("sequence ASC, id").Find(&allItems)
	if result.Error != nil {
		return false
	}
	for _, catValue := range allCats {
		if catValue.SyncID != "" {
			categoryUUIDperID[catValue.ID] = catValue.SyncID
			continue
		}
		var res []CategoryList
		SyncID := uuid.NewString()
		DB.Model(&res).Where("id = ?", catValue.ID).Update("SyncID", SyncID)
		categoryUUIDperID[catValue.ID] = SyncID
		if result.Error != nil {
			return false
		}
	}
	loadCategories()
	for _, itValue := range allItems {
		if itValue.SyncID != "" {
			itemUUIDperID[itValue.ID] = itValue.SyncID
			continue
		}
		var res []TodoItem
		catSyncID := categoryUUIDperID[itValue.CategoryListID]
		if catSyncID == "" {
			log.Println("Could not set item syncID")
			continue
		}
		SyncID := catSyncID + "_" + uuid.NewString()
		DB.Model(&res).Where("id = ?", itValue.ID).Update("SyncID", SyncID)
		itValue.SyncID = SyncID
		itemUUIDperID[itValue.ID] = SyncID
		if result.Error != nil {
			return false
		}
	}
	loadItems()
	return true
}
