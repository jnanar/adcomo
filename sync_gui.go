package main

import (
	"errors"
	"log"

	"fyne.io/fyne/v2/data/validation"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/widget"
)

func getSyncForm() *widget.Form {
	syncType := widget.NewRadioGroup([]string{"Bidirectionnal", "Remote to Local", "Local to Remote"}, func(value string) {
		log.Println("Radio set to", value)
	})
	syncUser := widget.NewEntry()
	syncPass := widget.NewPasswordEntry()
	enableBasicAuth := widget.NewCheck("Basic Auth", func(a bool) {
		EnableBasicAuth = a
	})
	syncURL := widget.NewEntry()
	syncURL.SetPlaceHolder("https://example.com/api/sync")
	syncURL.Validator = validation.NewRegexp(`[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)`, "not a valid URL")
	syncURL.Text = syncPlaceHolderURL
	syncUser.Text = syncPlaceHolderUser
	syncForm := &widget.Form{
		SubmitText: "Sync",
		Items: []*widget.FormItem{
			{Text: "URL", Widget: syncURL, HintText: "agayonTodo Sync endpoint"},
			{Text: "Auth", Widget: enableBasicAuth, HintText: "Shall we enable basic auth and use the "},
			{Text: "User", Widget: syncUser, HintText: "(requested if api behind basic authentication)"},
			{Text: "Password", Widget: syncPass, HintText: "(requested if api behind basic authentication)"},
			{Text: "Sync", Widget: syncType, HintText: "What do you want to sync ?"},
		},
		OnCancel: func() {
			FormWindow.Close()
		},
		OnSubmit: func() {
			syncPlaceHolderURL = syncURL.Text
			syncPlaceHolderUser = syncUser.Text
			username = syncUser.Text
			password = syncPass.Text
			if enableBasicAuth.Checked && (username == "" || password == "") {
				err := errors.New("username and password are required when basic auth is needed")
				dialog.ShowError(err, FormWindow)
				return
			}
			doFullSync(syncURL.Text, syncType.Selected)
			FormWindow.Close()
		},
	}
	return syncForm
}
