package main

import (
	"slices"

	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/widget"
)

var (
	resetOption []string
)

func getCleanForm() *widget.Form {
	checkGroup := widget.NewCheckGroup([]string{"Reset Categories", "Reset items"}, func(s []string) {
		resetOption = s
	})

	cleanForm := &widget.Form{
		SubmitText: "Reset",
		Items: []*widget.FormItem{
			{Text: "Reset", Widget: checkGroup, HintText: "Chose what you want to reset"},
		},
		OnCancel: func() {
			FormWindow.Close()
		},
		OnSubmit: func() {
			cnf := dialog.NewConfirm("Confirmation", "Do you want to delete these records ?",
				func(response bool) {
					redraw := false
					if response {
						if slices.Contains(resetOption, "Reset Categories") {
							var cats []CategoryList
							// Delete all records
							DB.Where("id > ?", 0).Delete(&cats)
							AppTabs.Items = nil
							AccordionPerCatID = make(map[int64]*widget.Accordion)
							loadCategories()
							redraw = true
						}
						if slices.Contains(resetOption, "Reset items") {
							var items []TodoItem
							// Delete all records
							DB.Where("id > ?", 0).Delete(&items)
							ItemsPerCats = make(map[int][]TodoItem)
							setItemsPerCat(AllCategories, AllItems)
							loadItems()
							redraw = true
						}
						if redraw {
							drawTabs()
						}

					}
				}, MainWindow)
			cnf.SetConfirmImportance(widget.DangerImportance)
			cnf.SetDismissText("No")
			cnf.SetConfirmText("Yes")
			cnf.Show()
			FormWindow.Close()
		},
	}
	return cleanForm
}
