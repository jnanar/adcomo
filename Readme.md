# Ad Como

Ad Como (To Do in latin) is a small Golang application written to handle simple todo lists.
It is based on the model of https://gitlab.com/jnanar/agayontodo.


![AdComo logo](Icon.png)

# Technicals details

Ad Como relies on the [fyne](https://www.mytinytodo.net/) Golang toolkit. It can be used to create categories (name of the tab) and items. Each item has a description that can be edited using [Markdown](https://docs.fyne.io/api/v2.4/widget/richtext.html) syntax.

If you already have an AgayonTodo instance, it is possible to sync the categories and items. Make sure to update your AgayonTodo instance to the latest [master](https://gitlab.com/jnanar/agayontodo/-/commit/939cf7c701ac71ce4d391defa6bbad745a416fc4) changes.

If your instance is behind a basic authentication mechanism, you can bypass it by enterpring your username and password.

## Installation

If you want to install the APK on your Android device, don't forget to enable [ADB debug](https://wiki.archlinux.org/title/Android_Debug_Bridge).

```sh
$ git clone git@gitlab.com:jnanar/adcomo.git
$ cd adcomo
$ # For desktop version
$ go build
$ # For Android
$  ~/go/bin/fyne package -os android -appID be.agayon.todo -icon Icon.png
$ adb install AdComo.apk 
```

See the [Fyne documentation](https://docs.fyne.io/started/mobile.html) for iOS.

# Captures

![Capture 1](capture.webp)
![Capture 2](capture2.webp)




