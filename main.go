package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/storage"
	"github.com/jimlawless/cfg"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

const (
	Version               = "v0.1-dev"
	configurationFilePath = "todo.conf"
	LogInfo               = "\t[Ad Como INFO]\t"
	LogError              = "\t[Ad Como  ERROR]\t"
	LogDebug              = "\t[Ad Como  DEBUG]\t"
)

var (
	mapConfig      = make(map[string]string)
	SqliteFilemane = "todo.db"
	DB             *gorm.DB
)

func init() {
	log.Printf("Running TODO API %v", Version)
	if !loadConfigFile() {
		log.Println("Failed to load configuration file.")
	}
}

func main() {
	log.Printf("Running Ad Como %v", Version)
	ComoApp = app.NewWithID("be.agayon.todo")
	databaseURI, err := storage.Child(ComoApp.Storage().RootURI(), SqliteFilemane)
	if err != nil {
		log.Fatal(err)
	}
	var databasePath string
	if mapConfig["config_file"] != "" {
		databasePath = "" + SqliteFilemane
	} else {
		databasePath = databaseURI.Path()
	}
	log.Printf("The database will be stored at %s", databaseURI.Path())

	db, err := gorm.Open(sqlite.Open(databasePath), &gorm.Config{})
	if err != nil {
		currentWorkingDirectory, err := os.Getwd()
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("Current Wroking Direcory: %s", currentWorkingDirectory)

		log.Panicln(err.Error())
		panic("failed to connect database")
	}
	db.AutoMigrate(
		CategoryList{},
		TodoItem{})
	// use Env variable to get populate mode
	if mapConfig["populate"] == "true" {
		Populate(db)
	}
	DB = db
	appRun()
}

func loadConfigFile() bool {
	ret := false
	for _, path := range strings.Split(".", ":") {
		log.Println("Try to find configuration file into " + path)
		configFile := path + "/" + configurationFilePath
		if _, err := os.Stat(configFile); err == nil {
			// The config file exist
			err = cfg.Load(configFile, mapConfig)
			if err == nil {
				// And has been loaded succesfully
				log.Println("Find configuration file at " + configFile)
				ret = true
				break
			}
		}
	}
	return ret
}
