package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"slices"
	"strings"
	"time"

	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/widget"
)

var (
	Client = &http.Client{
		Transport: &http.Transport{
			TLSHandshakeTimeout:   10 * time.Second,
			ResponseHeaderTimeout: 10 * time.Second,
			ExpectContinueTimeout: 6 * time.Second,
		},
	}
	syncPlaceHolderURL   = "http://agayon.test/api"
	syncPlaceHolderUser  = ""
	remoteCatSyncPerUUID = make(map[string]CategoryList)
	categorySyncPerUUID  = make(map[string]CategoryList)

	categoryUUIDperID = make(map[int64]string)
	itemUUIDperID     = make(map[int64]string)

	remoteItemSyncPerUUID = make(map[string]TodoItem)
	itemSyncPerUUID       = make(map[string]TodoItem)
)

// Limitations: The sync system is based on the record ids. It works when changes are made into one system and then another.
// If records are created at the same time on both systemes, they will share the same ID and only one record will be kept.
// It is not suitable in that situation. To improve that situation, a new field: sync_id should be created and used to keep track of
// the records on the remote database.
func doFullSync(urlString string, syncType string) {
	log.Printf("Sync content with %s...", urlString)
	// remove existing tabs content to redraw everything
	AppTabs.Items = nil
	// We make sure all local items and categories have syncID
	_setSyncID()
	// Make sure all remote items have a syncID
	var empty []byte
	doUpdateRequest(urlString, "/set_sync_id", empty)
	getCats(urlString, syncType)
	getItems(urlString, syncType)
	// reset everything
	ItemsPerCats = make(map[int][]TodoItem)
	AccordionPerCatID = make(map[int64]*widget.Accordion)
	setItemsPerCat(AllCategories, AllItems)
	drawTabs()
}

func doReadRequest(urlString string, urlpath string) (resp *http.Response, err error) {
	err = nil
	url, err := url.JoinPath(urlString, urlpath)
	if err != nil {
		dialog.ShowError(err, MainWindow)
		return
	}
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Fatal(err)
	}
	req.SetBasicAuth(username, password)
	// resp, err = Client.Get(url)
	resp, err = Client.Do(req)
	if err != nil {
		dialog.ShowError(err, MainWindow)
		return
	}
	if resp.StatusCode != 200 {
		err = errors.New("Could not fetch the data at " + url)
		dialog.ShowError(err, MainWindow)
		return
	}
	return
}

func doUpdateRequest(urlString string, urlpath string, json_data []byte) (resp *http.Response, err error) {
	err = nil
	url, err := url.JoinPath(urlString, urlpath)
	if err != nil {
		dialog.ShowError(err, MainWindow)
		return
	}
	payload := bytes.NewReader(json_data)
	req, err := http.NewRequest(http.MethodPost, url, payload)
	if err != nil {
		log.Fatal(err)
	}
	req.SetBasicAuth(username, password)
	resp, err = Client.Do(req)
	if err != nil {
		dialog.ShowError(err, MainWindow)
		return
	}
	if resp.StatusCode != 200 {
		err = errors.New("Could not fetch the data at " + url)
		dialog.ShowError(err, MainWindow)
		return
	}
	return
}

func doCreateRequest(urlString string, urlpath string, json_data []byte) (resp *http.Response, err error) {
	err = nil
	url, err := url.JoinPath(urlString, urlpath)
	if err != nil {
		dialog.ShowError(err, MainWindow)
		return
	}
	payload := bytes.NewReader(json_data)
	req, err := http.NewRequest(http.MethodPut, url, payload)
	if err != nil {
		log.Fatal(err)
	}
	req.SetBasicAuth(username, password)
	req.Header.Set("Content-Type", "application/json")
	resp, err = Client.Do(req)
	if err != nil {
		dialog.ShowError(err, MainWindow)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		dialog.ShowError(err, MainWindow)
	}
	log.Println(string(body))
	if resp.StatusCode != 200 {
		err = errors.New("Could not PUT the data at " + url)
		dialog.ShowError(err, MainWindow)
		return
	}
	return
}

func handleResp(resp *http.Response) (bodyBytes []byte, err error) {
	bodyBytes, err = io.ReadAll(resp.Body)
	if err != nil {
		dialog.ShowError(err, MainWindow)
		return
	}
	return
}

func getCats(urlString string, syncType string) {
	CatRes, err := doReadRequest(urlString, "read/category_list")
	if err != nil {
		dialog.ShowError(err, MainWindow)
		return
	}

	catBytes, err := handleResp(CatRes)
	if err != nil {
		dialog.ShowError(err, MainWindow)
		return
	}
	var catData []CategoryList
	err = json.Unmarshal(catBytes, &catData)
	if err != nil {
		dialog.ShowError(err, MainWindow)
		return
	}
	syncCats(urlString, catData, syncType)
}

func getItems(urlString string, syncType string) {
	itemRes, err := doReadRequest(urlString, "read/todo_item")
	if err != nil {
		dialog.ShowError(err, MainWindow)
		return
	}
	itBytes, err := handleResp(itemRes)
	if err != nil {
		dialog.ShowError(err, MainWindow)
		return
	}
	var itemData []TodoItem
	err = json.Unmarshal(itBytes, &itemData)
	if err != nil {
		dialog.ShowError(err, MainWindow)
	}
	syncItems(urlString, itemData, syncType)
}

func syncCats(urlString string, extData []CategoryList, syncType string) {
	var catsToUpdate []CategoryList
	var catsToCreate []CategoryList

	var remoteCatsToUpdate []CategoryList
	var remoteCatsToCreate []CategoryList

	var usedCatIUUID []string

	if syncType == "Bidirectionnal" || syncType == "Remote to Local" {
		for _, cat := range extData {
			// CurrentCategory is the local record
			CurrentCategory = categorySyncPerUUID[cat.SyncID]
			remoteCatSyncPerUUID[cat.SyncID] = cat
			if CurrentCategory.SyncID == "" {
				cat.ID = 0
				catsToCreate = append(catsToCreate, cat)
				usedCatIUUID = append(usedCatIUUID, cat.SyncID)
			} else if CurrentCategory.UpdatedAt.Before(cat.UpdatedAt) {
				// The remote item was updated after the local one. We need to fetch the changes
				cat.ID = CurrentCategory.ID // update is based on the ID primary key
				catsToUpdate = append(catsToUpdate, cat)
				usedCatIUUID = append(usedCatIUUID, cat.SyncID)
			}
		}
	}

	// Local to remote
	if syncType == "Bidirectionnal" || syncType == "Local to Remote" {
		for _, cat := range AllCategories {
			if slices.Contains(usedCatIUUID, cat.SyncID) {
				continue
			}
			remoteCat := remoteCatSyncPerUUID[cat.SyncID]
			if remoteCat.ID == 0 && remoteCat.Name == "" {
				cat.ID = 0
				remoteCatsToCreate = append(remoteCatsToCreate, cat)
			} else if remoteCat.UpdatedAt.Before(cat.UpdatedAt) {
				// The local item was updated after the local one. We need to update the change
				remoteCatsToUpdate = append(remoteCatsToUpdate, cat)
			}
		}
	}

	if len(catsToCreate) > 0 {
		res := DB.Create(catsToCreate)
		if res.Error != nil {
			dialog.ShowError(res.Error, MainWindow)
			return
		}
	}
	if len(catsToUpdate) > 0 {
		for _, cat := range catsToUpdate {
			var res []CategoryList
			_update(&res, cat.ID, cat)
		}
	}
	if len(remoteCatsToUpdate) > 0 {
		for _, cat := range remoteCatsToUpdate {
			updatedCatsStr, err := json.Marshal(cat)
			if err != nil {
				dialog.ShowError(err, MainWindow)
				return
			}
			doUpdateRequest(urlString, "/update/category_list/"+fmt.Sprint(cat.ID), updatedCatsStr)
		}

	}
	if len(remoteCatsToCreate) > 0 {
		for _, cat := range remoteCatsToCreate {
			createdCatsStr, err := json.Marshal(cat)
			if err != nil {
				dialog.ShowError(err, MainWindow)
				return
			}
			doCreateRequest(urlString, "/create/category_list/", createdCatsStr)
		}

	}
	loadCategories()
}

func syncItems(urlString string, extData []TodoItem, syncType string) {
	var itsToUpdate []TodoItem
	var itsToCreate []TodoItem

	var remoteItsToUpdate []TodoItem
	var remoteIttsToCreate []TodoItem

	var usedItemIDS []string

	// Remote to local
	if syncType == "Bidirectionnal" || syncType == "Out to In" {
		for _, it := range extData {
			// CurrentCategory is the local record
			localItem := itemSyncPerUUID[it.SyncID] // todo populate this !!! it is never filled
			remoteItemSyncPerUUID[it.SyncID] = it
			v := strings.SplitN(it.SyncID, "_", 2)
			catSyncID := v[0]
			localCat := categorySyncPerUUID[catSyncID]
			if localCat.ID == 0 {
				log.Println("Could not assign a category to item " + it.Name)
			}
			it.CategoryListID = localCat.ID
			it.CategoryList = localCat
			if localItem.ID == 0 && localItem.Name == "" {
				it.ID = 0
				itsToCreate = append(itsToCreate, it)
				usedItemIDS = append(usedItemIDS, it.SyncID)
			} else if localItem.UpdatedAt.Before(it.UpdatedAt) {
				// The remote item was updated after the local one. We need to fetch the changes
				it.ID = localItem.ID // update is based on id primary key
				itsToUpdate = append(itsToUpdate, it)
				usedItemIDS = append(usedItemIDS, it.SyncID)
			}
		}
	}
	// Local to remote
	if syncType == "Bidirectionnal" || syncType == "In to Out" {
		for _, it := range AllItems {
			if slices.Contains(usedItemIDS, it.SyncID) {
				continue
			}
			remoteItem := remoteItemSyncPerUUID[it.SyncID]
			if remoteItem.ID == 0 && remoteItem.Name == "" {
				it.ID = 0
				it.CategoryListID = 0
				remoteIttsToCreate = append(remoteIttsToCreate, it)
			} else if remoteItem.UpdatedAt.Before(it.UpdatedAt) {
				// The local item was updated after the local one. We need to update the change
				it.CategoryListID = 0
				remoteItsToUpdate = append(remoteItsToUpdate, it)
			}

		}
	}

	if len(itsToCreate) > 0 {
		res := DB.Create(itsToCreate)
		if res.Error != nil {
			dialog.ShowError(res.Error, MainWindow)
			return
		}
	}
	if len(itsToUpdate) > 0 {
		for _, it := range itsToUpdate {
			if it.CategoryList.ID == 0 {
				log.Println("Could not assign a category to item " + it.Name)
			}
			var res []TodoItem
			_update(&res, it.ID, it)
			_ = res
		}
	}
	if len(remoteItsToUpdate) > 0 {
		for _, it := range remoteItsToUpdate {
			it.ID = 0
			it.CategoryListID = 0
			updatedItsStr, err := json.Marshal(it)
			if err != nil {
				dialog.ShowError(err, MainWindow)
				return
			}
			doUpdateRequest(urlString, "/update/todo_item/"+fmt.Sprint(it.ID), updatedItsStr)
		}
	}
	if len(remoteIttsToCreate) > 0 {
		for _, it := range remoteIttsToCreate {
			createdItsStr, err := json.Marshal(it)
			if err != nil {
				dialog.ShowError(err, MainWindow)
				return
			}
			doCreateRequest(urlString, "/create/todo_item/", createdItsStr)
		}

	}
	loadItems()
}
